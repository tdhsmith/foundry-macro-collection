/*
  Calculate a simple XP estimate for selected tokens. Only applies to non-friendly actors.
*/

(async () => {
	let actors = canvas.tokens.controlled
			.filter(t => t.data.disposition < TOKEN_DISPOSITIONS.FRIENDLY)
			.map(t => t.actor);
	
	if (actors.length < canvas.tokens.controlled.length) {
		ui.notifications.warn(
			"Some selected tokens were omitted due to being friendly.");
	}
	
	new Dialog({
		title: "Party Level",
		content: `
			<label>Party Level</label>
			<input type="number" id="partylevel" name="partylevel" value="1">
		`,
		buttons: {
			calculate: {
				label: "Calculate",
				callback: html => {
					let level = parseInt(html.find("#partylevel").val(), 10);
					if (Number.isNaN(level)) {
						ui.notifications.error("Not a valid number");
					} else {
						calculateLevels(level);
					}
				}
			},
			cancel: {label: "Cancel"}
		}
	}).render(true);

	const OFFSET = 4;
	const SIMPLE_HAZARD_VALUES = [2, 3, 4, 6, 8, 12, 16, 24, 32];
	const NPC_VALUES = [10, 15, 20, 30, 40, 60, 80, 120, 160];
	
	function addEntry(map, level, label) {
		let key = `L${level} ${label}`;
		let obj = map.has(key) ? map.get(key) : {count: 0, xp: 0};

		obj.count++;
		map._count = (map._count || 0) + 1;
		if (!((level + OFFSET) in NPC_VALUES)) {
			ui.notifications.error(`Found actor outside party level ±4 (${level})-- unknown XP, skipping!`);
		} else {
			let xp = (label === "Simple Hazard")
				? SIMPLE_HAZARD_VALUES[OFFSET + level]
				: NPC_VALUES[OFFSET + level];
			obj.xp += xp;
			map._sum = (map._sum || 0) + xp;
		}
		map.set(key, obj);
	}

	function getLevel(target) {
		let level = target?.data?.data?.details?.level;
		if (Number.isSafeInteger(level)) return level;
		if (typeof level === "object" && Number.isSafeInteger(level.value)) return level.value;
		return NaN;
	}

	function calculateLevels(baseLevel) {
		let found = new Map();
		actors.forEach(a => {
			let level = baseLevel - getLevel(a);
			if (a.sheet.actorType === "hazard") {
				if (a.data.data.details.isComplex) {
					addEntry(found, level, "Complex Hazard");
				} else {
					addEntry(found, level, "Simple Hazard");
				}
			} else {
				// TODO: might be worth warning on unusual actorTypes here
				addEntry(found, level, "NPC");
			}
		});

		let tableHTML = Array.from(found)
			.sort((a, b) => a[0].localeCompare(b[0]))
			.map(([label, details]) => 
				`<tr><td>${label}</td><td>×${details.count}</td><td>${details.xp}</td></tr>`)
			.join("\n");
		console.log(tableHTML);
		new Dialog({
			title: "XP/Severity Estimate",
			content: `
				<em>Levels are relative to party level ${baseLevel}.</em>
				<table>
					<thead><tr><th>Adversary</th><th>Count</th><th>XP</th>
					<tbody>
						${tableHTML}
						<tr style="border-top: 1px solid">
							<th>TOTAL</th><td>${found._count}</td><td>${found._sum}</td>
						</tr>
					</tbody>
				</table>
				<br>
				Reference Encounter Budget <em>(Table 10-1, CRB p.489)</em>
				<table style="text-align: center">
				<thead><tr>
					<th>Threat</th>
					<th>XP Budget</th>
					<th>Player Adjustment</th>
				</tr></thead>
				<tbody>
					<tr><td>Trivial</td><td>≤40</td><td>≤10</td></tr>
					<tr><td>Low</td><td>60</td><td>15</td></tr>
					<tr><td>Moderate</td><td>80</td><td>20</td></tr>
					<tr><td>Severe</td><td>120</td><td>30</td></tr>
					<tr><td>Extreme</td><td>160</td><td>40</td></tr>
				</tbody>
				</table>
				`,
			buttons: {ok: {label: "OK"}}
		}).render(true);
	}
})();