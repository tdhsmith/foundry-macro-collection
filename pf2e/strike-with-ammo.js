/*
  Simultaneously roll a weapon attack and deduct ammunition.
*/
// Configure these:
const weapon_name = "Hand Crossbow";
const ammo_name = "Bolts";
const MAP = [0, -5, -10];
const announce_remaining = true;
const a = token.actor; // use selected
// alternatively...
// const a = game.actors.get("id")
// const a = game.actors.find(a => a.name === "name")

const weapon = a.items.find(i => i.name.includes(weapon_name));
const ammo = a.items.find(i => i.name === ammo_name);


function strike (map) {
	let e = new Event("macro");
	let quant = ammo.data.data.quantity.value;
	if (quant === 0) {
		ui.notifications.warn("No ammunition remaining!")
	} else {
		weapon.rollWeaponAttack(e, map);
		a.updateOwnedItem(
			{_id: ammo.id, data: {quantity: {value: quant - 1}}});
		if (announce_remaining) {
			ChatMessage.create({content: `${a.name} has ${quant -1} ${ammo_name.toLocaleLowerCase()} remaining.`});
		}
	}
}

new Dialog({
	title: `${weapon_name} strike with ammo`,
	content: `Select your current MAP:`,
	buttons: {
		first: {
			label: MAP[0].toString(),
			callback: () => strike(MAP[0])
		},
		second: {
			label: MAP[1].toString(),
			callback: () => strike(MAP[1])
		},
		third: {
			label: MAP[2].toString(),
			callback: () => strike(MAP[2])
		}
	}
}).render(true);