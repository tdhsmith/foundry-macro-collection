/* 
This macro exports important Pathfinder Society reporting details for characters.
It it currently designed to align with the format of the excellent Pathfinder Society
Chronicle Filler (pfscf) by Blesmol, available at https://github.com/Blesmol/pfscf
However, it is designed to be easily modified to collect any data in a delimited format.
*/

let prestige = 1; // base XP and rep gain for this adventure
let bySelected = true; // whether to use currently selected tokens, or all user characters
let separator = ";";

// some reference consts
// value of a treasure bundle per level
const treasure = [null, 1.4, 2.2, 3.8, 6.4, 10, 15, 22, 30, 44, 60];
const questMultiplier = 2.5;
const factionNames = {
  EA: "Envoy's Alliance",
  GA: "Grand Archive",
  HH: "Horizon Hunters",
  RO: "Radiant Oath",
  VS: "Vigilant Seal",
  VW: "Verdant Wheel"
};

// map of output keys to either their static value or a callback run on each actor
let actorValues = {
  gmid: 2389885,
  event: "Online PFS 2e with Taylor",
  date: (new Date()).toDateInputString(),
  xp: prestige,
  fac1_rep_gained: prestige,
  societyid: a => `${a.data.data.pfs.playerNumber}-${a.data.data.pfs.characterNumber}`,
  fac1_name: a => factionNames[a.data.data.pfs.currentFaction],
  gp: a => questMultiplier * treasure[a.data.data.details.level.value] + "gp",
  player: a => game.users.find(u => u.character === a)?.name,
  fac1_rep_total: a => a.data.data.pfs.reputation[a.data.data.pfs.currentFaction] + prestige
};

let keys = Object.keys(actorValues);
let actors = bySelected ?
  canvas.tokens.controlled.map(t => t.actor) :
  game.users.map(u => u.character).filter(c => c);

if (actors.length === 0) {
  ui.notifications.warn(`No valid actors found.`);
} else {
  let table = keys.map(k => {
    if (typeof actorValues[k] === "function") {
      // this is a dynamic value using a callback
      let evaluated = actors.map(a => actorValues[k](a));
      console.warn(evaluated);
      return [k].concat(evaluated);
    } else {
      // this is the same value for all actors
      return [k].concat((new Array(actors.length)).fill(actorValues[k]));
    }
  });
  let csv = table.map(row => row.join(separator)).join("\n");

  new Dialog({
    title: "PFS CSV Output",
    content: "<textarea id='pfsout' width='100%' height='100%' rows='16'></textarea>",
    buttons: {done: {label: "Done"}},
    render: html => html.find("textarea").val(csv)
  }, {width: 600}).render(true);
}