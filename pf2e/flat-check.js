let showAdditional = true;  // set to false if having the additional uses makes the dialog too big / cluttered
// data for building the table
let DCs = [
  {dc: 20, color: "#fc9c85", primary_use: "", additional_uses: ""},
  {dc: 19, color: "#fda181", primary_use: "", additional_uses: ""},
  {dc: 18, color: "#fea57d", primary_use: "", additional_uses: ""},
  {dc: 17, color: "#feab7a", primary_use: "", additional_uses: "sticky poison, fortification rune, stone bones, vigorous health, stubborn persistence"},
  {dc: 16, color: "#fdb077", primary_use: "", additional_uses: "talisman cord, saving slash, unexpected shift"},
  {dc: 15, color: "#fbb574", primary_use: "recover from <em>persistent damage</em>", additional_uses: "steady spellcasting"},
  {dc: 14, color: "#f9bb72", primary_use: "", additional_uses: "greater fortification rune"},
  {dc: 13, color: "#f6c171", primary_use: "recovery check (dying 3)", additional_uses: ""},
  {dc: 12, color: "#f2c770", primary_use: "recovery check (dying 2)", additional_uses: "mist child"},
  {dc: 11, color: "#eecc71", primary_use: "recovery check (dying 1), target <em>hidden</em> creature, recover from <em>confused</em> after damage", additional_uses: "erratic time, shield sconce, silencing strike"},
  {dc: 10, color: "#e9d272", primary_use: "assisted recovery from <em>persistent damage</em>, overcharge a wand", additional_uses: "keen eyes, oil, ward casting, unstable concoction"},
  {dc: 9, color: "#e4d874", primary_use: "", additional_uses: "bag of devouring, supernatural senses"},
  {dc: 8, color: "#dede78", primary_use: "cast while <em>stupefied 3</em>", additional_uses: "major ancestral curse"},
  {dc: 7, color: "#d7e47c", primary_use: "cast while <em>stupefied 2</em>", additional_uses: ""},
  {dc: 6, color: "#d0e982", primary_use: "cast while <em>stupefied 1</em>", additional_uses: "impeded magic, moderate ancestral curse, augury, dancing rune, roll with it, mist child"},
  {dc: 5, color: "#c8ef88", primary_use: "target <em>concealed</em> creature, manuipulate while <em>grabbed</em>, auditory while <em>deafened</em>", additional_uses: "major lore curse, spiritual disruption, silence heresy, monitor lizard support, sticky poison, blind-fight, archer's aim, sense allies, revealing stab, pale lavender ellipsoid, black pearl, blackfinger blight, buoyancy vest, dust pod, sovereign steel armor, slough skin, spirit's interference"},
  {dc: 4, color: "#bff490", primary_use: "", additional_uses: "minor ancestral curse"},
  {dc: 3, color: "#b6fa98", primary_use: "structure on unstable surface", additional_uses: "keen eyes, supernatural senses, obsidian steed, signifier's sight"},
  {dc: 2, color: "#acffa2", primary_use: "", additional_uses: ""}
];

let rowsHTML = DCs.map(dc => `<tr style="background-color: ${dc.color};" data-dc="${dc.dc}">
<td style="padding: 5px">${dc.dc}</td><td title="${dc.additional_uses}">${dc.primary_use ? "<strong>" + dc.primary_use + "</strong>": "—"} <small>${dc.additional_uses && showAdditional ? "<br>" + dc.additional_uses : ""}</small></td>
</tr>`).join("\n");

let flatCheckDialog = new Dialog({
    title: "Flat Check",
    content: `
<table border="1px solid" style="text-align: center;">
    <thead>
        <tr><th>DC</th><th>Common Uses</th></tr>
    </thead>
    <tbody>
        ${rowsHTML}
    </tbody>
</table>
<p>Simply click a row to roll that DC.</p>
`,
    buttons: {cancel: {label: "Cancel"}},
    render: html => {
        html.find("tr").click(ev => {
            rollFlatCheck(ev.target.closest("tr")?.dataset?.dc);
            flatCheckDialog.close();
        });
    } 
}, {width: 600}).render(true);

function rollFlatCheck(dc, flavor="flat check vs") {
    dc = parseInt(dc, 10);    
    if (Number.isNaN(dc)) return;
    let r = new Roll(`1d20cs>=${dc}`);
    r.roll();
    let outcomeMsg = `<strong style="color: ${r.total > 0 ? "darkgreen" : "darkred"}">${r.total > 0 ? "success" : "failure"}</strong>`;
    r.toMessage({flavor: `${flavor} DC${dc} : ${outcomeMsg}`}).then(m => console.warn(m));
}

    r.toMessage({flavor: `${flavor} DC${dc} : ${outcomeMsg}`}).then(m => console.warn(m));
}
