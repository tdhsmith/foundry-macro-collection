/* stubs of macros / proof-of-concepts I've written for people on Discord */

/* ---- skip to next track in playlist ---- */
// assumes there is at most 1 active playlist
let playlist = game.playlists.find(p => p.playing)
let track = playlist?.sounds?.find(s => s.playing);
if (track) {
  let howl = playlist.audio[track._id].howl;
  howl.seek(howl.duration());
}


/* ---- swap selected doors' types secret<->normal ---- */
(async () => {
for (let wall of canvas.walls.controlled) {

    if (wall.data.door == WALL_DOOR_TYPES.SECRET)
        wall.update({"door": WALL_DOOR_TYPES.DOOR});
    else if (wall.data.door ==  WALL_DOOR_TYPES.DOOR) {
        // this extra step is only necessary due to a bug in
        // foundry that fails to remove door controls on type change
        // so we convert to wall first and *then* to secret
        await wall.update({"door": WALL_DOOR_TYPES.NONE});
        wall.update({"door": WALL_DOOR_TYPES.SECRET});
    }
}
})();


/* ---- open file picker to replace image of current tile ---- */
let tile = canvas.tiles.controlled[0];
if (tile instanceof Tile) {
    new FilePicker({
        type: "image",
        current: tile.data.img,
        callback: imagePath => tile.update({img: imagePath}),
        displayMode: "thumbs"
    }).browse(tile.data.img);
}


/* ---- do something at the beginning of each round of combat ---- */
let lastRoundSeen = new Map();
let hookNumber = Hooks.on("updateCombat", combat => {
  let lastRound = lastRoundSeen.get(combat._id) ?? 0;
  if (combat.round > lastRound) {
    ui.notifications.warn(`Round ${combat.round} started!`);
    lastRoundSeen.set(combat._id, combat.round);
  }
});
console.warn(
  "remove hook with command: ",
  `Hooks.off("updateCombat", ${hookNumber});`);
