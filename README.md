Collection of macros for [Foundry VTT](https://foundryvtt.com/). Mostly with an emphasis on Pathfinder 2e, but some other systems & general applicability.

If you have questions / comments, feel free to raise issues here or ping me on the Foundry Discord server.